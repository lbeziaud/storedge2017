# Distributed storage solutions for the edge computing paradigm

Louis Béziaud supervised by Philippe Bonnet. 3-months internship at the IT University of Copenhagen, 2017

see [report](report/storadge2017_report.pdf) and [slides](slides/storadge2017_slides.pdf)

## Abstract

The proliferation of Internet of Things and the success of rich cloud services have pushed the horizon of a new computing paradigm, Edge computing, which calls for processing the data at the edge of the network. Similar to Cloud, Edge computing provides data, computing, storage, and application services to end-users. Edge computing has the potential to address the concerns of response time requirement, battery life constraint, bandwidth cost saving, as well as data safety and privacy. Such paradigm requires shifting the storage facilities to the edge of the backbone in order to bring Cloud computing resources closer to the end-usage. Although the infrastructures and technologies used in centralized Cloud storage are well studied, the design of a decentralized storage at the Edge has not yet been discussed. The recent evolution of the storage hierarchy has seen the emergence of new technologies such as Open-channel SSDs, which allow the separation of host, storage and controller. In this paper we argue that this storage architecture is well fitted for the Edge computing paradigm. We also explore how existing approaches can be used to design the infrastructure needed for Edge storage.