\section{Decentralized Storage}\label{sec:decentralized}

In this section we first describe the basis of a decentralized storage
infrastructure and underline the key differences with its centralized
counterpart. We then review recent software that propose such a
system.

We consider a city-wide decentralized network of heterogeneous
nodes---similar to the DFC described in
Section~\ref{sec:architecture}---at the edge of the network. Contrary
to the centralized model, the network is composed of multiple units
grouping hosts, controllers and disks, as depicted on
Figure~\ref{fig:topology}. While devices inside a same unit trust each
other, two different units do not.

The base requirements of a storage service is that any node of the
system must be able to add and get data from the store. Other common
properties include replication of data, access
management~\cite{muller2017data}, etc.

\begin{figure}[h]
  \centering
  \input{topology.tikz}
  \caption{Example of network topology considered in this paper}
  \label{fig:topology}
\end{figure}

\subsection{Discovering and Exchanging}

Multiple software provide the building block for a decentralized
storage system, that is a way to discover and exchange files between
peers.

\paragraph{InterPlanetary File System}
InterPlanetary File System~\cite{benet2014ipfs} (IPFS) is a protocol
designed to create a permanent and decentralized method of storing and
sharing files. It is inspired by the technologies of BitTorrent and
Git.

IPFS is transport agnostic, so it can run over any transport protocol
(eg.~TCP, UDP), and on any Internet architecture (eg.~NDN~\cite{ndn},
XIA~\cite{xia}). It defines a content-addressable (ie.~a resource is
retrieved based on its content, not its location) network overlay.

Files in IPFS are immutable. Similar to the Git version manager,
instead of ``replacing'' a file, the new version is added to the
network, with a pointer to the original version.

IPFS uses a block storage logic---it emulates the behavior of a
traditional block device, such as a physical hard drive, and storage
is organized as sequence of bytes having a maximum length--and
provides a file system abstraction.

\paragraph{Tahoe Least-Authority File Store}
Tahoe Least-Authority File Store~\cite{selimi2014tahoe} (Tahoe-LAFS)
is a decentralized data store and file system designed around the
``principle of least privilege''. It uses cryptographic capabilities
to handle read, write and verify access on mutable and immutables
files and directories. A capability is a communicable, unforgeable
token of authority. It refers to a value that references an object
along with an associated set of access rights. The capabilities
provide a content-addressable network.

Tahoe-LAFS makes use of with Reed-Solomon~\cite{reed1960polynomial}
erasure coding to provide RAID-like replication. Tahoe-LAFS assumes
that two users willing to share data are able to discover themselves
and securely exchange capabilities.

\paragraph{GlusterFS}
GlusterFS~\cite{glusterfs} is a scale-out network-attached storage
POSIX file system. Functionalities include file-based mirroring and
replication, file-based striping, file-based load balancing, volume
failover, scheduling and disk caching, storage quotas, and volume
snapshots with user serviceability. GlusterFS follows a client-server
architecture, where servers are viewed as pool by a client. Network
discovery is unspecified. It is assumed the servers inside a pool are
trusted.

\paragraph{Infinit}
Infinit~\cite{infinit} is a distributed key-value store built on top of
an overlay network and a distributed hash table by aggregating storage
capacity from a collection of servers. It provides resilience through
redundancy and fault tolerance.

% \todo{OceanStore}~\cite{kubiatowicz2000oceanstore}
% \todo{Cassandra}~\cite{lakshman2010cassandra}

\subsection{Blockchain}
For a storage platform to provide replication, access control, or
other form of constrained storage, there must be a way for a node to
ask for other nodes to store its data.

The absence of trust between participants introduce the need for a
consensus algorithm. Using a consensus protocol, such as
Paxos~\cite{Lamport1978Time}, to achieve agreement among the peers can
incur substantial overhead when coordination is required in a
Byzantine setting.

Blockchain~\cite{sun2016blockchain,shafagh2017towards,hari2016internet}
has been introduced in 2008 by Nakamoto as a way to obtain a relaxed
consensus in an untrusted asynchronous decentralized environment. The
blockchain algorithm in itself is outside the scope of this work, we
only give a brief overview of the protocol.

A blockchain is a protocol in which each user possesses a variable
state containing blocks. Blocks themselves contains a set of messages
and a header pointing at the most recently created block before
them. The blocks thus form a chronologically ordered chain. There are
three types of nodes in the network: players, who create messages and
broadcasts them to the network; miners who build blocks by collecting
the messages they receive (once a block is mined, this block is
broadcast within the network); users who add the blocks they receive
into the blockchain if and only if all the messages they contain are
valid.

In order to preserve the unicity of the chain shared by all users, a
blockchain protocol provides a deterministic algorithm to select a
particular branch---thus transforming a tree back into a chain--,
along with an algorithm, such as proof-of-work~\cite{dwork1992pricing}
or proof-of-stake~\cite{bentov2016cryptocurrencies}, which allows
blocks to be added to the chain in a desynchronised fashion, while
protecting from denial of service attacks and abuses.

\subsection{Constrained Storage}

Multiple blockchain have been proposed as way to incentive nodes to
host each other's data. In these systems, a client need to spend
tokens (i.e. some form of currency) to host its data, whereas a host
receive tokens for offering its disk space.

\paragraph{}%
\emph{Storj}~\cite{wilkinson2014storj},
\emph{Sia}~\cite{vorick2014sia}, and
\emph{Filecoin}~\cite{protocol2017filecoin} are the most known
decentralized marketplace for Cloud storage using a blockchain. The
three protocols use a blockchain as an open ledger of contracts
between users. A contract is established each time a user need to send
data into the system.

Storj uses a trusted platform as a gateway into the system. The
platform receives hosting requirements from a client and forward them
to available peers.

The need for a centralized platform is mainly due to the way Storj
ensures a client that its data is safely stored and on some peer's
disk and available. To verify the availability and integrity, the
platform asks the host to compute a checksum on a subset of the data,
which is compared to a pre-computed checksum. This follows a heartbeat
protocol.

Sia and Filecoin makes uses of smart contracts to enforce redundancy
and availability. Smart contracts are small programs executed by every
participant of a blockchain. A smart contract need to respect the
properties of a blockchain: every peer, at any time, must obtain the
same output and be able to verify the output. In Sia or Filecoin, a
smart contract is associated to a transaction between a client and
some hosts. For example, the smart contract can state that payment
will be made (automatically) to the hosts if a given file is hosted
for a given period of time.

\paragraph{MaidSAFE} is another storage marketplace. In MaidSAFE, tokens a paid on
data retrieval. The amount of tokens someone can earn is directly
linked to the resources they provide and how often their computer is
turned on.

% \paragraph{Filecoin}\cite{protocol2017filecoin} is a market in which clients
% spend tokens for storing and retrieving data while miners earn tokens
% by storing and serving data. Smart contracts, based on Ethereum's
% smart contracts, are deployed in the network to let users write their
% own conditions for storage as well as design reward strategies for
% miners, among other things.

% Proof-of-Replication is a Proof-of-Storage which allows a server to
% convince a user that some data has been replicated to its own
% storage. This proof mitigates Sybil attacks, Outsourcing attacks, and
% Generation attacks.

% Proof-of-Spacetime allow a user to check if a storage provider is
% storing the outsourced data at the time of the challenge.

% Smart Contracts based on Ethereum.

% Files have an expiration date, after which no reward (payment?) is
% offered for storing the file.

% Relative disk space is used as power in the
% proof-of-retrievability. As the difficulty increases (i.e. with time
% and system usage), the disk space increases, causing small miners to
% be excluded from the system. This leads to centralization pressure.

% The files included in proof-of-retrievability are chosen
% determinastically. Unavailability of a file would lead to the death of
% a file \todo{claimed by KopperCoin}.

Permacoin~\cite{miller2014permacoin} and
KopperCoin~\cite{kopp2016koppercoin} are two blockchains designed for
storage, which do not make use of a proof-of-work during the block
creating. This makes the schemes energy efficient.

Ethereum’s Swarm was conceived of as a storage protocol tailored for
interoperation with the Ethereum smart contract ecosystem. It makes
uses of the Ethereum’s consensus proces. Swarm takes the approach of
rewarding nodes for serving content. Because more often requested
content is more profitable to store than rarely requested content,
rewarding nodes only for recall would incentivise the trashing of
rarely accessed data.

% \paragraph{BigchainDB}~\cite{mcconaghy2016bigchaindb}

% \subsection{Distributed Hash Table}
% \cite{oortwijn2016distributed,dragojevic2014farm,mitchell2013using,kalia2014using,szepesi2014designing}

%% Local Variables:
%% mode: latex
%% TeX-master: "main"
%% End:
